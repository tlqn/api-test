﻿using System;
using System.Linq;
using System.Threading.Tasks;
using SharpBucket.V2;
using SharpBucket.V2.Pocos;
using SharpBucket.V2.EndPoints;

namespace CalanceBBApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var sharpBucket = new SharpBucketV2();
            sharpBucket.BasicAuthentication("quynhu.tranlam@gmail.com", "abcxyz098");

            var userEndPoint = sharpBucket.UsersEndPoint("tlqn");
            // querying the Bitbucket API for various info
            var userProfile = userEndPoint.GetProfile();

            var repositoriesEndPoint = sharpBucket.RepositoriesEndPoint();
            //// getting the Repository resource for a specific repository
            var repositoryResource = repositoriesEndPoint.RepositoryResource("tlqn", "API Test");
            //// getting the list of all the commits of the repository
            var commits = repositoryResource.ListCommits();
            commits.Reverse();
            System.IO.File.WriteAllText(@"C:\Users\Lam Tran\Desktop\Git\api-test\CalanceBB.txt", "");
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@"C:\Users\Lam Tran\Desktop\Git\api-test\CalanceBB.txt", true))
                foreach (Commit i in commits)
                {
                    file.WriteLine(i.message + i.date);
                }

     
            // create the pull request
            //var pullRequestsResource = repositoryResource.PullRequestsResource();
            //var pullRequestToApprove = new PullRequest
            //{
            //    title = "a good work to approve",
            //    source = new Source { branch = new Branch { name = "master" } }
            //};

            //var pullRequest = pullRequestsResource.PostPullRequest(pullRequestToApprove);
            //var pullRequestResource = pullRequestsResource.PullRequestResource(pullRequest.id.GetValueOrDefault());
            //var approvalResult = pullRequestResource.ApprovePullRequest(); 
        }
    }
}
